""" 
Text class and functions

Used to hold all of the stuff that you actually read.
Neat, right? Not really, this class doesn't have to
be with unless you want to add some functionality.

Written by Wyatt J. Miller
"""

# TODO: build out class, methods and functions, writing things out comes later

import os
import sys
import time
from . import race, classes, utils

class Text(object):

    def __init__(self):
        self.util = utils.Utilities()

    def chooseGender(self):
        """Text fucntion that has you choose your gender."""
        choseGender = None
        
        def x():
            choseGender = input("Male or Female? ")
        
            # TODO: fix conditional, only accepts Male input
            if choseGender != "Male" or choseGender != "Female" or choseGender != "male" or choseGender != "female":
                print("There are only two genders. Please try again.")
                x()

        def printError():
            print("There can only be two genders. Please try again!")

        x()

        return choseGender 

    def chooseRace(self):
        """Text method that has you choose your race."""
        # TODO: build out method
        r = race.Race()
        print("RACES:")
        print(r.ListAllRaces())
        choseRace = input("Choose your race: ")

        # TODO: conditonal goes here for validation

    def chooseClass(self):
        """Text method that has you choose your class."""
        c = classes.Class()
        print("CLASSES:")
        print(c.ListAllClasses())
        choseClass = input("Choose your class: ")

        # TODO: conditonal goes here for validation

    def exitConformation(self):
        x = input("Are you sure you want to leave?")

        if x == "Y" or "y":
            sys.exit(0)
        elif x == "N" or "n":
            # TODO: return to main menu, still have to build
            pass
        else:
            Text().exitConformation()

    def mainMenu(self):
        # TODO: building out, def needs more work
        self.util.clearScreen()
        print("==========================")
        print("   Character Creator v1   ")
        print("==========================")
        print()
        print("1. Create")
        print("2. Credits")
        print("3. Exit")
        print()
        x = input("ENTER A FIGURE: ")
        # TODO: conditional here :p