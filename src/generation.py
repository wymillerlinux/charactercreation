"""
Generation class

Generates your stats for you automagically.

Written by Wyatt J. Miller
"""

import os
import sys
import time
import random
#from . import player, classes

class Generation:

    def __init__(self, is_running, is_stable, race, cl, randomness_special_attribuite):
        self.randomness_special_attribute = random.randrange(1, 10)
        self.is_running = False
        self.is_stable = False
        self.race = None
        self.cl = None

        # TODO: this function doesn't run like I want it to. Refactoring is needed.
        if self.is_running == False:
            self.is_running = True
        elif self.is_running == True:
            self.is_stable = True
        elif self.is_running == True and self.is_stable == True:
            print("Generator v1.0.0 is ready. Written by Wyatt J. Miller")
        else:
            print("Generator v1.0.0 is not ready.")