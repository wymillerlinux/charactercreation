"""
Race classes

These cool cats of classes are models
of races a user can choose from. Currently,
there are six. Maybe I'll add more?

Written by Wyatt J. Miller
"""

import os
import sys
import time
from . import enums

# Base class
class Race(object):

    def __init__(self):
        self.race = None
        self.races = enums.RaceChooser
        self.special_attribute_race = None
        self.attribute_stat = 0

    def ListAllRaces(self):
        for x in self.races:
            print(x) # TODO: needs to look prettier

# Child classes
class Human(Race):

    def __init__(self):
        self.race = self.races.HUMAN
        self.special_attribute_race = "Honor"
        self.attribute_stat = 5
        
class Dwarf(Race):

    def __init__(self):
        self.race = self.races.DWARF
        self.special_attribute_race = "Diginity"
        self.attribute_stat = 7

class DarkElf(Race):

    def __init__(self):
        self.race = self.races.DARKELF
        self.special_attribute_race = "Negotiation"
        self.attribute_stat = 6

class Unicorn(Race):

    def __init__(self):
        self.race = self.races.UNICORN
        self.special_attribute_race = "Pride"
        self.attribute_stat = 2

class Orc(Race):

    def __init__(self):
        self.race = self.races.ORC
        self.special_attribute_race = "Smell"
        self.attribute_stat = 9

class Vampire(Race):

    def __init__(self):
        self.race = self.races.VAMPIRE
        self.special_attribute_race = "Surprise"
        self.attribute_stat = 5
