import os
import sys
import time
from . import error

class Utilities(object):

    def __init__(self):
        self.err_one = error.OperatingSystemError("OS not supported! OS error thrown!", 1)

    def clearScreen(self):
        if sys.platform == "linux" or sys.platform == "darwin":
            os.system("clear")
        else:
            self.err_one