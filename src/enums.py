from enum import Enum

class RaceChooser(Enum):
    """
    Enum that holds the races in the 
    character creation tool.
    """
    
    NONE = None
    HUMAN = "Human"
    DWARF = "Dwarf"
    DARKELF = "Dark Elf"
    UNICORN = "Unicorn"
    ORC = "Orc"
    VAMPIRE = "Vampire"

class ClassChooser(Enum):
    """
    Enum that holds all the races in the
    character creation tool.
    """

    NONE = None
    WARRIOR = "Warrior"
    ROUGE = "Rouge"
    MAGE = "Mage"
    SCOUT = "Scout"
    BARD = "Bard"
    PREIST = "Priest"

class Sex(Enum):
    """
    Enum that holds the difference between X and Y chromesomes.
    There are only two genders lolz. 
    """
    
    NONE = None
    MALE = "Male"
    FEMALE = "Female"
    GAY = "Gay"
    LESBIAN = "Lesbian"
    TRANS = "Transgender"
    QUEER = "Queer"
    PAN = "Pansexual"
    APACHE = "Apache Attack Helicopter"

class MenuOptions(Enum):
    """Enum that holds the menu options."""
    # TODO: build this sucker out, this is way tougher than I imagined
    pass