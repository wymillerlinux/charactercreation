"""
Error classes

This class defines errors that are specific
to this program. Error away!

Written by Wyatt J. Miller
"""

import os
import sys
import time

class OperatingSystemError(Exception):
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors

class ValidationError(Exception):
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors