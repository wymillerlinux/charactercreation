"""
Controller class

Controls all the things!

Written by Wyatt J. Miller
"""

import os
import sys
import time
from . import race, classes, text, generation

class Controller:

    # TODO: build out class, coming soon
    def __init__(self):
        self.is_running = False
        self.is_stable = False

    def controller_init(self):
        self.is_running = True
        self.r = race.Race()
        self.c = Controller()
        self.cl = classes.Class()
        self.t = text.Text()
        # self.g = generation.Generation()

    def controller_loop(self):
        self.c.controller_init()
        
        while self.is_running == True:
            
            print("I'm running!")
            
            if self.is_running == False:
                print("Cleaing up...")
                sys.exit(0)

