import os
import sys
import time
from . import enums

class Class(object):
    """
    The Class object holds all the base classes for the character
    creation tool. Derives from the Object object. This class defines things
    that all classes have in common.
    """

    def __init__(self):
        self.cl = None
        self.classes = enums.ClassChooser
        self.special_attribute_class = None
        self.attribuite_stat = 0

    def ListAllClasses(self):
        for cl in self.classes:
            print(cl + "\n") # TODO: needs to look prettier

class Warrior(Class):
    """
    Based of the Class object, this one of the classes, the Warrior. Super strong
    and handsome.
    """
    
    def __init__(self):
        self.cl = self.classes.WARRIOR
        self.special_attribute_class = "Super Slash"

class Rouge(Class):
    """
    Based of the Class object, this one of the classes, the Rouge. This class
    will steal all of your shit. 
    """

    def __init__(self):
        self.cl = self.classes.ROUGE
        self.special_attribute_class = "Sneak"

class Mage(Class):
    """
    Based of the Class object, this one of the classes, the Mage. Master of conjuring.
    """

    def __init__(self):
        self.cl = self.classes.MAGE
        self.special_attribute_class = "Conjour"

class Scout(Class):
    """
    Based of the Class object, this one of the classes, the Scout. Quick on his/her feet.
    """

    def __init__(self):
        self.cl = self.classes.SCOUT
        self.special_attribute_class = "Speed"

class Bard(Class):
    """
    Based of the Class object, this one of the classes, the Bard. Bitch of
    all the classes in this tool. Doesn't do shit.
    """

    def __init__(self):
        self.cl = self.classes.BARD
        self.special_attribute_class = "Music"

class Priest(Class):
    """
    Based of the Class object, this one of the classes, the Priest. Essential party member
    because he/she HEALS PEOPLE!
    """

    def __init__(self):
        self.cl = self.classes.PREIST
        self.special_attribute_class = "Heal"
